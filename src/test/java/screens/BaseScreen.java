package screens;

import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;

public class BaseScreen {

    @AndroidFindBy(id = "ru.beru.android:id/nav_profile")
    protected SelenideElement profileIcon;

    @Step("Нажать иконку профиля")
    public BaseScreen clickOnProfile() {
        profileIcon.click();
        return this;
    }
}
