package screens;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;

public class ProfileScreen extends BaseScreen {
    @AndroidFindBy(id = "ru.beru.android:id/profileLoginButton")
    private SelenideElement loginButton;

    @AndroidFindBy(xpath = "//*[contains(@text, 'Выход')]")
    private SelenideElement logOutButton;

    @Step("Нажать кнопку \"Войти\"")
    public ProfileScreen clickLoginButton() {
        loginButton.shouldBe(Condition.visible).click();
        return this;
    }

    @Step("Нажать кнопку \"Выход\"")
    public ProfileScreen clickLogOutButton() {
        logOutButton.shouldBe(Condition.visible).click();
        return this;
    }
}
