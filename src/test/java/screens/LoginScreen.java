package screens;

import com.codeborne.selenide.*;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;

import java.time.Duration;

public class LoginScreen {
    @AndroidFindBy(xpath = "//*[contains(@text, 'Почта')]")
    private SelenideElement loginByMailButton;

    @AndroidFindBy(className = "android.widget.EditText")
    private SelenideElement loginField;

    @AndroidFindBy(xpath = "//*[@resource-id='passp-field-passwd']")
    private SelenideElement passwordField;

    @AndroidFindBy(xpath = "//*[@resource-id='passp:sign-in']")
    private SelenideElement loginButton;

    @AndroidFindBy(xpath = "//*[@resource-id='field:input-login:hint']")
    private SelenideElement loginErrorMessageField;

    @AndroidFindBy(xpath = "//*[@resource-id='field:input-passwd:hint']")
    private SelenideElement passwordErrorMessageField;

    @AndroidFindBy(xpath = "//*[@resource-id='ru.beru.android:id/profileUserNameTextView']")
    private SelenideElement profileUsername;

    @Step("Выбрать метод авторизации через почту")
    public LoginScreen chooseMethodByMail() {
        loginByMailButton.shouldBe(Condition.visible, Duration.ofSeconds(10))
                .click();
        return this;
    }

    @Step("Заполнить поле логина значением: {login}")
    public LoginScreen setLogin(String login) {
        loginField.setValue(login);
        return this;
    }

    @Step("Заполнить поле пароля значением: {password}")
    public LoginScreen setPassword(String password) {
        passwordField.setValue(password);
        return this;
    }

    @Step("Нажать кнопку \"Войти\"")
    public LoginScreen clickLoginButton() {
        loginButton.shouldBe(Condition.visible, Duration.ofSeconds(10)).click();
        return this;
    }

    @Step("Получить текст сообщения об ошибке пароля")
    public String getPasswordError() {
        return passwordErrorMessageField.getText();
    }

    @Step("Получить текст сообщения об ошибке логина")
    public String getLoginError() {
        return loginErrorMessageField.getText();
    }

    @Step("Получить имя пользователя из Профиля")
    public String getLoginFromProfile() {
        return profileUsername.getText();
    }
}
