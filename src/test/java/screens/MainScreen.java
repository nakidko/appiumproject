package screens;

import com.codeborne.selenide.*;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.NoSuchElementException;

import java.time.Duration;

public class MainScreen extends BaseScreen {
    @AndroidFindBy(id = "ru.beru.android:id/closeButton")
    private SelenideElement closeChooseCityWindowButton;

    @AndroidFindBy(id = "ru.beru.android:id/positiveButton")
    private SelenideElement acceptCookiesButton;

    @AndroidFindBy(id = "ru.aliexpress.buyer:id/anotherMethodsButton")
    private SelenideElement chooseAnotherWayLink;

    @AndroidFindBy(xpath = "//*[contains(@text, 'Почта')]")
    private SelenideElement loginByMailButton;

    @Step("Закрыть промо окно и принять куки")
    public MainScreen closeStartWindows() {
        try {
            closeChooseCityWindowButton.shouldBe(Condition.visible,
                    Duration.ofSeconds(6)).click();
        } catch (NoSuchElementException e) {
            System.out.println(e.getMessage());
        }
        try {
            acceptCookiesButton.shouldBe(Condition.visible,
                    Duration.ofSeconds(6)).click();
        } catch (NoSuchElementException e) {
            System.out.println(e.getMessage());
        }
        return this;
    }
}
