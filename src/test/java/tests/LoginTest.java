package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.LoginScreen;
import screens.MainScreen;
import screens.ProfileScreen;

import static com.codeborne.selenide.appium.ScreenObject.screen;
import static config.Configs.LOGIN;
import static config.Configs.PASSWORD;

public class LoginTest extends BaseTest {

    private final String PASSWORD_ERROR_MESSAGE = "Неверный пароль";

    private final String LOGIN_ERROR_MESSAGE = "Нет такого аккаунта. " +
            "Проверьте логин или войдите по телефону";

    private String failedLogin = faker.name().username();

    private String failedPassword = faker.internet().password();

    @BeforeMethod(description = "Перейти на экран авторизации")
    public void goToLoginScreen() {
        MainScreen mainScreen = screen(MainScreen.class);
        mainScreen
                .closeStartWindows()
                .clickOnProfile();

        ProfileScreen profileScreen = screen(ProfileScreen.class);
        profileScreen.clickLoginButton();
    }

    @Test(description = "Успешная авторизация")
    @Description("Описание теста: Проверка авторизации с валидными данными")
    @Feature("Авторизация")
    @Severity(SeverityLevel.CRITICAL)
    public void successLoginTest() {
        LoginScreen loginScreen = screen(LoginScreen.class);
        loginScreen
                .chooseMethodByMail()
                .setLogin(LOGIN)
                .clickLoginButton()
                .setPassword(PASSWORD)
                .clickLoginButton();

        Assert.assertEquals(loginScreen.getLoginFromProfile(), LOGIN,
                "Не отображается Логин / авторизация не удалась");
    }

    @Test(description = "Авторизация с невалидным логином")
    @Description("Описание теста: Проверка авторизации с невалидным логином")
    @Feature("Авторизация")
    @Severity(SeverityLevel.NORMAL)
    public void loginWithInvalidLoginTest() {
        LoginScreen loginScreen = screen(LoginScreen.class);
        loginScreen
                .chooseMethodByMail()
                .setLogin(failedLogin)
                .clickLoginButton();

        Assert.assertEquals(loginScreen.getLoginError(), LOGIN_ERROR_MESSAGE,
                "Текст сообщения об ошибке" + loginScreen
                        .getLoginError() + "Не соответствует "
                        + LOGIN_ERROR_MESSAGE);
    }

    @Test(description = "Авторизация с невалидным паролем")
    @Description("Описание теста: Проверка авторизации с невалидным паролем")
    @Feature("Авторизация")
    @Severity(SeverityLevel.CRITICAL)
    public void loginWithInvalidPasswordTest() {
        LoginScreen loginScreen = screen(LoginScreen.class);
        loginScreen
                .chooseMethodByMail()
                .setLogin(LOGIN)
                .clickLoginButton()
                .setPassword(failedPassword)
                .clickLoginButton();

        Assert.assertEquals(loginScreen.getPasswordError(),
                PASSWORD_ERROR_MESSAGE, "Текст сообщения об ошибке"
        + loginScreen.getPasswordError() + "не соответствует "
                        + PASSWORD_ERROR_MESSAGE + "1");
    }
}
