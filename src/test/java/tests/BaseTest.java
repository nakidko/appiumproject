package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.appium.SelenideAppium;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.github.javafaker.Faker;
import driver.AndroidDriverProvider;
import io.qameta.allure.Step;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public abstract class BaseTest {
    Faker faker = new Faker();

    @Step("Открыть приложение")
    @BeforeMethod(description = "Подготовить и открыть приложение")
    public void setUp() {
        Configuration.timeout = 6000;

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                        .screenshots(true)
                        .savePageSource(false));

        Configuration.browser = AndroidDriverProvider.class.getName();
        SelenideAppium.launchApp();
    }

    @Step("Закрыть приложение")
    @AfterMethod(description = "Закрыть приложение")
    public void closeApp() {
        WebDriverRunner.closeWebDriver();
    }
}
