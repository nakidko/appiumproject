package driver;

import com.codeborne.selenide.WebDriverProvider;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.UiAutomator2Options;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

import javax.annotation.CheckReturnValue;
import javax.annotation.Nonnull;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class AndroidDriverProvider implements WebDriverProvider {
    @Nonnull
    @Override
    @CheckReturnValue
    public WebDriver createDriver(@Nonnull Capabilities capabilities) {
        String absoluthAppPath = new File("src/main/resources/app/Yarket.apk")
                        .getAbsolutePath();
        UiAutomator2Options options = new UiAutomator2Options();
        options
                .setPlatformName("Android")
                .setAutomationName("UiAutomator2")
                .setDeviceName("Pixel_4_API_30")
                .setApp(absoluthAppPath)
                .setAppPackage("ru.beru.android")
                .setAppActivity("ru.yandex.market.activity.main.MainActivity")
                .setUdid("emulator-5554")
                .fullReset();

        try {
            return new AndroidDriver(
                    new URL("http://127.0.0.1:4723"), options);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
